sudo apt install libxml2-utils
myvar=$(echo 'cat //coverage[@lines-covered]/@line-rate' | xmllint --shell coveragereport/Cobertura.xml | awk -F'[="]' '!/>/{print $(NF-1)}')
final=`echo "$myvar * 100 " | bc `
echo $final
echo ${final%.*}

if [ "${final%.*}" -gt 20 ]
then
   echo "Code coverage OK"
else
   echo "Code coverage not ok"
   exit 1
fi