module.exports = function (config) {
  config.set({
  basePath: '../../',

  frameworks: [
      'jasmine'
  ],

  // list of files / patterns to load in the browser
  files: [
      'app/js/**/*.js',
      'test/spec/**/*.js'
  ],

  preprocessors: {
      'app/js/**/*.js': ['coverage']
  },

  exclude: [],

  reporters: [ 'progress', 'junit', 'coverage' ],

  coverageReporter: {
      type: 'cobertura',
      dir: 'coverage/'
  },

  junitReporter: {
      outputFile: 'test/reports/unit/junit/junit.xml',
      suite: 'unit'
  },

  port: 9876,

  runnerPort: 9100,

  colors: true,

  logLevel: config.LOG_DEBUG,

  autoWatch: false,

  browsers: [ 'Chrome' ],

  captureTimeout: 60000,

  singleRun: true
});
};